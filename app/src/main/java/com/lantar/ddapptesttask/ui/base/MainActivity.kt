package com.lantar.ddapptesttask.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import com.lantar.ddapptesttask.R
import com.lantar.ddapptesttask.data.model.User
import com.lantar.ddapptesttask.data.model.clearAll
import com.lantar.ddapptesttask.data.model.readUsers
import com.lantar.ddapptesttask.data.network.onResponseUsers
import com.lantar.ddapptesttask.data.network.updateUsers
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*


class MainActivity : AppCompatActivity(), onResponseUsers {

    val list: ArrayList<String>? = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        for (user in readUsers()) {
            list!!.add(user.id.toString() + " " + user.login.toString())
        }

        if (list!!.size == 0)
            updateDb()
        else
            updateView()

        fab_update.setOnClickListener({
            updateDb()

        })


        fab_clear.setOnClickListener({
            clearAll()
            list!!.clear()
            updateView()
        })


    }

    fun updateDb() {
        updateUsers(this, this)
        progressBar.setVisibility(ProgressBar.VISIBLE)
    }

    open fun updateView() {
        progressBar.setVisibility(ProgressBar.GONE)
        listView.adapter = ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                list)

    }

    override fun usersResponse(users: List<User>) {
        progressBar.setVisibility(ProgressBar.GONE)
        list!!.clear()
        for (user in users) {
            list!!.add(user.id.toString() + " " + user.login.toString())
        }
        updateView()
    }


}




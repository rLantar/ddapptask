package com.lantar.ddapptesttask.data.model;


import io.realm.Realm;
import io.realm.exceptions.RealmMigrationNeededException;

/**
 * Created by Lantar on 11/20/2017.
 */

public class RealmSingleton {
    private static RealmSingleton instance;

    public Realm realm = Realm.getDefaultInstance();


    public static synchronized RealmSingleton getInstance() {
        if (instance == null) {
            instance = new RealmSingleton();
        }
        return instance;
    }

    public Realm getRealm() {
        return realm;
    }

    public void setRealm(Realm realm) {
        this.realm = realm;
    }

    public void buildDatabase(){

        try {
            this.realm =  Realm.getInstance(Realm.getDefaultConfiguration());
        } catch (RealmMigrationNeededException e){
            try {
                Realm.deleteRealm(Realm.getDefaultConfiguration());
                //Realm file has been deleted.
                this.realm =  Realm.getInstance(Realm.getDefaultConfiguration());
            } catch (Exception ex){
                throw ex;
                //No Realm file to remove.
            }
        }
    }
}
package com.lantar.ddapptesttask.data.network

import com.lantar.ddapptesttask.data.model.User
import retrofit2.http.GET
//import retrofit.http.Path
import rx.Observable


/**
 * Created by Lantar on 11/18/2017.
 */
interface GitHubService {

    @GET("users?per_page=100")
    fun listUsers(): Observable<List<User>>


}
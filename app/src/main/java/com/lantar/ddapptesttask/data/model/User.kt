package com.lantar.ddapptesttask.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
 * Created by Lantar on 11/18/2017.
 */

@RealmClass
open class User: RealmObject(){

    @PrimaryKey
    @SerializedName("id")
    @Expose
    open var id: Int = 0

    @SerializedName("login")
    @Expose
    open var login: String? = null

    @SerializedName("url")
    @Expose
    open var url: String? = null

    @SerializedName("avatar_url")
    @Expose
    open var avatar_url: String? = null


}
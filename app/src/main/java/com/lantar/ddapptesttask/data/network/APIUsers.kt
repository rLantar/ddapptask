package com.lantar.ddapptesttask.data.network

import android.content.Context
import android.util.Log
import com.lantar.ddapptesttask.data.model.saveUsers
import io.realm.Realm
import rx.android.schedulers.AndroidSchedulers

import rx.schedulers.Schedulers

/**
 * Created by Lantar on 11/18/2017.
 */

fun updateUsers(mContext: Context, onResponseUsers: onResponseUsers) {


    val client = getGitHubServiceRx(mContext)

    val realm = Realm.getDefaultInstance()
    client.listUsers()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                    { users ->
                        onResponseUsers.usersResponse(users)
                        saveUsers(users)

                    },
                    { error ->
                        Log.e("Error", error.message)
                    }
            )
    realm.close()
}



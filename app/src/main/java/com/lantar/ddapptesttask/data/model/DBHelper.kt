package com.lantar.ddapptesttask.data.model

import io.realm.Realm


/**
 * Created by Lantar on 11/19/2017.
 */

fun saveUsers(listUsers: List<User>) {

    var realm = RealmSingleton.getInstance().realm

    for (user in listUsers) {
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(user)
        realm.commitTransaction()
    }
}

fun readUsers(): List<User> {

    var realm = RealmSingleton.getInstance().realm

    val query = realm.where(User::class.java)

    val savedUserd = query.findAll()


    return savedUserd
}

fun clearAll() {

    RealmSingleton.getInstance().realm.close()
    Realm.deleteRealm(Realm.getDefaultConfiguration())
    RealmSingleton.getInstance().buildDatabase()



}
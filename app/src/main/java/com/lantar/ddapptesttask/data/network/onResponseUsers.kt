package com.lantar.ddapptesttask.data.network

import com.lantar.ddapptesttask.data.model.User

/**
 * Created by Lantar on 11/20/2017.
 */
interface onResponseUsers {
    fun usersResponse(users: List<User>)
}